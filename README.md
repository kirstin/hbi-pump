![Photo of the HBI syringe pump](/Images/HBI_pump.jpg "")


# The Helsinki Bioimaging 3D-printed Syringe Pump 
is open source and the main body consists of 4 3D-printed parts which can be easily printed with smaller 3D printers (maximum part length 8.5 cm). It is controlled via an Arduino and can be used together with an peristaltic pump for example in NanoJ-Fluidics.


Documentation: https://gitlab.com/kirstin/hbi-pump

DOI: 10.5281/zenodo.7917675

To be used together with a fork of NanoJ-Fluidics with a few small modifications required for stepper-motors: https://gitlab.com/kirstin/nanoj-fluidics.

![3D model of HBI syringe pump](/Images/HBI_pump.gif )

3D-View of the pump: https://a360.co/3tQit4E


## What are the differences to other open source syringe pumps?

This pump was inspired by the Poseidon pump from the Patcher lab. The main difference is that the Helsinki Bioimaging syringe pump is split up in several parts that can be easily printed on a smaller 3D printer and can be easily assembled.
Further changes are:
- Addition of a small motor support (optional)
- Rod clamps are replacing the setscrews
- Slighty shorter threaded rod. This functions as a safety mechanism, so that the sledge hopes down the rail instead of pressing against the frame 
- Hole to access the motor coupling was moved to the bottom to increase stability 
- T-slot nut in the sledge
- Different syringe mount
- 3D printable case for the Arduino + motor shield


In addition, with our Arduino code the stepper syringe pump can be run together with a DC motor peristaltic pump on the same motor shield.
For this the Arduino code includes a motor class with a common simple interface for controlling stepper- and dc-motors connected to Adafruit Motorshields. By this any number and combination of stepper- and DC-motors can be controlled.

Currently, as a quick and easy solution, our Arduino code is programmed so that it functions together with NanoJ-Fluidics.
The original NanoJ-Fluidics does round values to whole seconds. Therefore, we slightly adapted it to send time in milliseconds to the Arduino and thus work more precise. 

Note: To fully make use of the advantage of stepper motors of a large range of speeds (very slow to very fast), a seperate motor-controller should be developed for NanoJ-Fluidics.


## What do you find here?

- CAD files of all 3D printed components for the syringe pump
- CAD files for accessories:
    - Arduino housing
    - Tube spiral to preheat medium in the microscope incubator (in case the syringe pump is stored outside)
    - Syringe mount (to hold the syringe in place and distribute the pressure from the screw more even on the syringe)
    - M5 square nut (for the screw that holds the syringe. It has a better fit than a hex nut. We made a threat into it with a screw tap)
    - 35 mm dish dummy with holes for tubing (to get tubing in and out the incubation chamber if you have multiple positions for dishes)
- Arduino firmware to control the pump via NanoJ-Fluidics
- Slightly modified version of NanoJ-Fluidics for running the syringe pump on low speed and high precision. 
- Python/Jupyter Notebook script to calculate the "speedConversion"-factor and syringe calibration fitting best to your application (Calc_Calibration)
- List of materials (incl. suppliers) for the pump and the hardware control



A fork of NanoJ-Fluidics with a few small modifications required for stepper-motors can be found on https://gitlab.com/kirstin/nanoj-fluidics.

## Building the syringe pump
The Helsinki Bioimaging syringe pump is an open source, DIY alternative to commercial systems. In addition to the 3D-printed parts you will need a few common components that can be easily purchased.
This syringe pump can be used for microfluidic experiments in combination with a peristaltic pump or one its own. 
After you have plug the three main parts together and glued the motor support in place the building process is rather similar to the Poseidon pump. As the Patcher lab has documented this very well, we recommend to have a look at https://pachterlab.github.io/poseidon/ in case you have doubts about the assembly process.
Please notice that in our design the side rods sit very tight and you might not be able to get them back out once inserted.

### 3D-printing the components 

This project has been optimized for printing on a Makerbot Method X with 0.2 mm tolerance and a build volume of 190 x 190 x 196 mm. We think that this design should work for other printers as well, but we have not tested it. So we are happy to hear about your experience with other printers.

If you want to use a printer with less tolerance we recommend you to add a little bit of tolerance. Due to the tight tolerances in our design it is enough to just plug the joints of the three main parts together. If you have a looser fit some additional gluing might be required.
In case you have a printer with less tolerance but a large enough building volume you might be quicker printing the Poseidon pump developed by the Patcher lab (https://pachterlab.github.io/poseidon/).

For the best stability/flexibilty of the rod clamps we recommend to print the parts in the orientation as shown below: 

![Recommended print orientation](/Images/printing_orientation.jpg "Recommended print orientation")



Printing material was ABS, which offers the advantage that it can be easily glued with Acetone. Other materials should work as well.
In the CAD there are holes in the front and back of the syringe pump to clamp the rods. We only inserted screws in the back part. The front part was already fitting very tight. In case you want to have a looser fit (easier assembly), you might however need to include a screw to clamp the rods at the front part as well. 




## Developers

The Helsinki Bioimaging Syringe pump was created at the Biomedicum Imaging Unit at the University of Helsinki by Kirstin Vonderstein.


## Prior work and references

The pump design was based on the open source syringe pump published by the Pachter lab

Documentation: https://pachterlab.github.io/poseidon/

Publication: A. Sina Booeshaghi, Eduardo da Veiga Beltrame, Dylan Bannon, Jase Gehring and Lior Pachter, Principles of open source bioinstrumentation applied to the poseidon syringe pump system, Scientific Reports 9, Article number: 12385 (2019), https://doi.org/10.1101/521096 

The Enclosure for the Arduino + motorshield was based on the Arduino Uno Enclosure by Taylor Stein:
https://gallery.autodesk.com/fusion360/projects/21486/arduino-uno-enclosure

For design of all 3D-components Fusion360 (free academic license) was used. 

As a user interface we used a slightly modified version of NanoJ-Fluidics developed by Henriques lab, and
the matching serial interface in the arduino code was also taken from NanoJ-Fluidics. (https://github.com/HenriquesLab/NanoJ-Fluidics/)

Documentation: https://github.com/HenriquesLab/NanoJ-Fluidics/wiki/GUI-Home
Publication: Almada, P., Pereira, P.M., Culley, S. et al. Automating multimodal microscopy with NanoJ-Fluidics. Nat Commun 10, 1223 (2019). https://doi.org/10.1038/s41467-019-09231-9



## License
This work is licensed under a BSD 2-Clause license.

Please cite as 10.5281/zenodo.7917675 Kirstin Vonderstein, Helsinki Bioimaging 
