#ifndef SimpleMotor_h
#define SimpleMotor_h

#include <Wire.h>
#include <AccelStepper.h>
#include <Adafruit_MotorShield.h>

/* Simple Motor
 * 
 * Author: Kirstin Vonderstein, Helsinki Bioimaging, 2021
 * 
 * SimpleMotor is a generic motor class without functionality defining a
 * simplified run-time based interface for running motors connected to Adafruit_MotorShield. 
 * It is designed to operate both Stepper and DC-Motors with a common interface.
 * SimpleStepper and SimpleDCMotor are specific child classes controlling Stepper/DC motors with a common interface.
 * SimpleStepper is based on AccelStepper for running Steppers in a non-blocking way.
 * Both DC and stepper are externally controlled by the runtime & speed, 
 * but for steppers the runtime and speed is recalculated to number steps.
 * It is important to run the doRunning() method as often as possible to get a good time resolution especially for high speeds
*/


//MotorTypes:s
#define SIMSTEPPER 1
#define SIMDC 2
#define MAX_STEP_SPEED 200

/* Generic Motor Base Class (not functional) *************************************************** 
 *  Just defining the generic interface.
 */
class SimpleMotor{
public:
  SimpleMotor();

  virtual void setSpeedConversion(float speedConversion);
  virtual void setSpeed(float){return;}
  virtual void setRunTime(long newRunTime){return;}
  
  virtual void run(uint8_t){return;} //Start a motor run with set Speed and RunTime

  virtual bool doRunning(void){return true;} //Realtime control of run (stopps motor after targettime), returns true if motor stopped

  virtual float getTimeToGo(void){return 0;} //Returns set speed
  virtual uint8_t getType(void); //Returns motor Type
  virtual float getSpeed(void){return 0;} 
  
  protected:
  uint8_t motorType;
  float speedConversion;
};


/*STEPPER Class***************************************************
 * 
 * This class is based on the AccelStepper class using its functionality for non-blocking stepper control.
 * (Note: A lot of functionality of AccelStepper is unused, so the used functionality could be directly implemented to save memory and increase speed)
 * The number of steps is calculated from the targetRunTime (setRunTime) and the stepperSpeed (setSpeed) 
 * 
 * Runs at maximum at ca. 250 steps/s on Arduino UNO...
 * 
 * Possible QUICK FIX not tested: isMaxSpeed is used to check if speed is set to 255, and runs the pump at max speed (200 steps/seconds) 
 * better directly send a broader speed range (nut just 0-255) from control software
 */
static void dummyFun(void){} //dummy function to use AcellStepper constructor

class SimpleStepper: private AccelStepper, public SimpleMotor {
public:
  SimpleStepper(Adafruit_MotorShield *MC,uint16_t steps, uint8_t num, float speedConversion=1, uint8_t AFStepStyle=SINGLE);
  
  //Overriding step function to use basic AF Stepper function
  //This avoids using function pointers...
  void step(long step); 
  
  void setSpeed(float);
  void setRunTime(long newRunTime);
  void run(uint8_t);
  
  bool doRunning(void);//Returns true if motor stopped
  float getTimeToGo(void);
  float getSpeed(void);
  
private:
  unsigned long targetRunTime;
  unsigned long targetSteps;
  float stepperSpeed;
  
  //float _speed; // Steps per second
  Adafruit_StepperMotor *AFStepper;
  uint8_t AFStepStyle;
  bool StepperStatus;

  //Quick fix:
  //bool isMaxSpeed;  //Returns true if speed is max. Reason: To be able to have a fast way to move sledge to put syringe in and out
};


/*DC-Motor Class***************************************************
 * uses  Adafruit_DCMotor functions to run DC motors
 */
class SimpleDCMotor: public SimpleMotor {
public:
  SimpleDCMotor(Adafruit_MotorShield *MCShield,uint8_t num, float speedConversion=1);
  
  void run(uint8_t cmd);
  void setSpeed(float);
  void setRunTime(long newRunTime);
   
  bool doRunning(void); //Returns true if motor stopped
  float getTimeToGo(void);
  float getSpeed(void);
  
private:
  unsigned long targetRunTime;
  uint8_t DCSpeed;
  
  //Run control:
  unsigned long startTime;
  unsigned long elapsedTime;
  bool DCMotorStatus;

  Adafruit_DCMotor *AFDCMotor;
};



#endif 
