/* 
Stepper/DC motor pump firmware Version 1.0

Author: Kirstin Vonderstein, Helsinki Bioimaging, 2021
The interface to NanoJ-Fluidics is based on the code from Pedro Almada, UCL, 2015

This is the firmware for an Arduino Uno to drive DC motor and stepper pumps through
the Adafruit Motor Shield v2, while under control of MicroManager/Fiji NanoJ-Fluidics
plugins (or standalone Java). See: https://github.com/HenriquesLab/NanoJ-Fluidics 

The pumped volume for both DC and stepper are controlled by the runtime & speed, but for steppers 
the runtime and speed is recalculated to number steps.
The current version is somewhat limited by the NanoJ-Fluidics and a seperate pump driver suitable for both stepper and DC motors should be made in NanoJ-Fluidics.

ToDos:
*Change speed input to a larger range to allow for very slow speeds possible with stepper motors.
(Currently the range is chosen by "speedConversion" when configuring the motors, motor[nShields][nMotorsPerShield])

*The SimpleMotor.h could be made independent from the AccelStepper library.

*For Arduino Uno the maximum stepper speed is around 250 steps/s, it could be limited by the complicated use of AccelStepper library (needs to be confirmed!).

*Currently the code runs only a single pump at a time. 
It is possible to start for example the DC motor (e.g. perestaltic pump) always if another pump is started (example code around line 257).

Commands:
g = get status of all pumps
a = stop all pumps
p = get number of pumps
axy = stop pump xy
sxynnn = for pump xy set speed nnn
rxydtttttttt - Start pump xy in direction d for tttttttt milliseconds 
	(!important change from original version since the stepper motor has much higher resolution!)

The commands use this notation:
d = 1 is forward, d = 2 is backwards
x = shield address {1-nShields}
y = motor/pump address {1-nMotorsPerShield}
nnn = speed {000-255} Comment: This is currently limiting the dynamic range of the speed for steppers and should be changed!
tttttttt = duration {00001-99999}
*/


//// Imports
#include <EEPROM.h>
// Adafruit Motor Shield libraries
#include <Wire.h>
#include <AccelStepper.h>
#include <Adafruit_MotorShield.h>
#include "SimpleMotor.h"
//// Settings

// Number of shields (MAX: 9)
const int nShields = 1;
// Number of DC motors per shield (MAX on Adafruit Motor Shield v2: 4 DC or 2 Steppers)
const int nMotorsPerShield = 2; //Max numbers of motors per shield (2 if only stepper, 4 if DC and Steppers)

//*************** Setup motorshields ***************************************
  // We now create a Motor Shield object for each shield. Their address 
  // (96-128) is set by soldering a few jumpers.
  // see: https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/stacking-shields
  // Note here we are using decimal values for the address which is simpler
  // but their tutorial uses hex.
Adafruit_MotorShield afms[nShields]={Adafruit_MotorShield(96)}; //nShields=3=> {Adafruit_MotorShield(96),Adafruit_MotorShield(97),Adafruit_MotorShield(98)}


SimpleMotor DummyMotor=SimpleMotor();//Dummy motor for empty slots: use &DummyMotor for filling empty motor slots if less then nMotorsPerShield are connected
/*************** Setup pumps ***************************************
//Steppers are setup by   new SimpleStepper(Adafruit_MotorShield *MC, uint16_t steps, uint8_t num, [float speedConversion=1, uint8_t AFStepStyle=SINGLE])
//DCMotors are setup by   new SimpleDCMotor(Adafruit_MotorShield *MC, uint8_t num, [float speedConversion=1])
//*MC       is the motorshield, (e.g. &afms[0] for the first motorshield)
//steps     is the number of steps per revolution
//num       is the moter number on the shield: For steppers it can be 1 or 2, for DC motors 1,2,3 or 4. (a stepper uses two DC ports)
//speedConversion (optional) is a factor multiplied to the setSpeed input. For DC motors speeds are numbers between 0 and 255 and the factor should be 1, 
//          but stepper motors speeds are in steps per seconds. E.g. for maximum 127 steps per second and minimum of 0.5 steps per second use speedConversion of 0.5...
//AFStepStyle is the stepper mode from the (see Adafruit_MotorShield documentation) MICROSTEP,SINGLE,INTERLEAVE ...

//Add motors: For each motorshield "nMotorsPerShield" have to be added! 
//Add &DummyMotor for empty slots in the motor array (this is important for mixing DC&stepper motors):
*/
SimpleMotor* motor[nShields][nMotorsPerShield]={
  {new SimpleStepper(&afms[0],200,1,0.2,INTERLEAVE),   new SimpleDCMotor(&afms[0],3)}                                             //Motors on afms[0] 1*stepper + 1*DC   nMotorsPerShield = 4 add dummy motors:",    &DummyMotor,    &DummyMotor"
  //{new SimpleStepper(&afms[1],200,1),   new SimpleDCMotor(&afms[1],3),    new SimpleDCMotor(&afms[1],4),    &DummyMotor}  //Motors on afms[1] 1*stepper + 2*DC  for nMotorsPerShield = 4
  //{new SimpleStepper(&afms[2],200,1),   new SimpleStepper(&afms[1],200,2),&DummyMotor,    &DummyMotor}                    //Motors on afms[2] 2*stepper         for nMotorsPerShield = 4
};

//*******************************************************************

// Character which signals end of message from PC
const byte endMessage = '.';


// Misc. initializations
byte currentShield;
byte currentMotor;
bool bFinished=false;
int eeAddress = 0;
const int maxMessageLength = 12; //12
const int MAX_INPUT=30;
char incoming[maxMessageLength];
unsigned long startTime = 0;
unsigned long elapsedTime = 0;
double targetTime = 0;
long targetSteps =0; //Target steps of current motor

boolean timeCounter = false;


// The Arduino should keep track of what the motors have been told to do. 
// We can address each motor's status by accessing the values on motorStatus
// Format is: [shield][motor][speed/state]
// Speed can vary from 0 to 255
// States are: 0 - stop; 1 - forward; 2 - backward.
int motorStatus[nShields][nMotorsPerShield][2] = {{}};

void setup() {
  // Start serial communication and set baud-rate
  Serial.begin(57600);
  Serial.println("Connected!");

  for (int i=0; i<nShields; i++) {
    // Initialize shield objects with the default frequency 1.6KHz
    afms[i].begin();
  }

  // Note: Removed motor inizialization loop and hardcoded above, since different number of motors can be connected to a shield.

}

void loop() {
  
  
  //Run current pump (other pumps are inactive!)
  if (timeCounter == true) {
      bFinished=motor[currentShield][currentMotor]->doRunning(); //This line has to run as often as possible especially for fast stepper speeds.
            
      // If current pump has finished, we stop also all other pumps
      if (bFinished){         
          for (int i=0; i < nShields; i++) {
           for (int j=0; j < nMotorsPerShield; j++) {
                motorStatus[i][j][1] = 0;
                EEPROM.put(eeAddress, motorStatus);
                motor[i][j]->run(RELEASE); //Release stepper motor, less warming, but doesn't actively hold position
                //Serial.println(motor[currentShield][currentMotor]->getTimeToGo());
                //Serial.println("stepping!");
             }
           }
           timeCounter=false;
           //Serial.println("Finished stepping!");
        }
  }


  //non-blocking serial read:
  if (Serial.available () > 0)
    processIncomingByte(Serial.read());
 
}



//Process cmd and do action
void processSerialCmd (const char * incoming, int lengthOfMessage)
  {
     // If incoming message is:
     // g = get status of all pumps
     if (incoming[0] == 'g') {
       for (int i=0; i < nShields; i++) {
         for (int j=0; j < nMotorsPerShield; j++) {
           Serial.print("S");
           Serial.print(i + 1);
           Serial.print("M");
           Serial.print(j + 1);
           Serial.print(":");
           Serial.print(motorStatus[i][j][0]);
           Serial.print(",");
           Serial.print(motorStatus[i][j][1]);
           Serial.print(";");
         }
       }
     Serial.println();
     }
     // a = stop all pumps and release all stepper motors
     else if (incoming[0] == 'a' && lengthOfMessage == 1) {
       for (int i=0; i < nShields; i++) {
         for (int j=0; j < nMotorsPerShield; j++) {
           motorStatus[i][j][0] = 255;
           motorStatus[i][j][1] = 0;
           EEPROM.put(eeAddress, motorStatus);
           motor[i][j]->run(RELEASE);
         }
       }
       elapsedTime = 0;
       timeCounter = false;
       Serial.print("Stopped all pumps!");
       Serial.println();
     }
     
     // axy = stop pump xy
     else if (incoming[0] == 'a' && lengthOfMessage != 1) {
       currentShield = incoming[1] - '0' - 1;
       currentMotor  = incoming[2] - '0' - 1;
       motorStatus[currentShield][currentMotor][1] = 0;
       EEPROM.put(eeAddress, motorStatus);
       motor[currentShield][currentMotor]->run(RELEASE);

       elapsedTime = 0;
       timeCounter = false;
       Serial.print("Stopped pump: ");
       Serial.print(incoming[1]);
       Serial.print(",");
       Serial.print(incoming[2]);
       Serial.print("!");
       Serial.println();
     }
     
     //p = get number of pumps (not all might be connected!)
     else if (incoming[0] == 'p') {
      String s = String(nShields, DEC);
      String m = String(nMotorsPerShield, DEC);
      String result = String(s + "." + m);
      Serial.print(result);
      Serial.println();
     }
     
     // sxynnn = for pump xy set speed nnn
     else if (incoming[0] == 's') {
       currentShield = incoming[1] - '0' - 1;
       currentMotor  = incoming[2] - '0' - 1;
       byte a = incoming[3] - '0';
       byte b = incoming[4] - '0';
       byte c = incoming[5] - '0';
       byte currentSpeed = (a*100)+(b*10)+c;
             
       motorStatus[currentShield][currentMotor][0] = currentSpeed;
       EEPROM.put(eeAddress, motorStatus);
       motor[currentShield][currentMotor]->setSpeed(currentSpeed); 
       
       Serial.print("Set speed of pump: ");
       Serial.print(incoming[1]);
       Serial.print(",");
       Serial.print(incoming[2]);
       Serial.print(" to ");
       Serial.print(motor[currentShield][currentMotor]->getSpeed());
       Serial.println(" steps/s.");
     }
     
     // rxydtttttttt - Start pump xy in direction d for tttttttt milli seconds 
     // d = 1 is forward, d = 2 is backwards
     else if (incoming[0] == 'r' && lengthOfMessage == maxMessageLength) {
       char targetTimeInput[8] = {incoming[4],incoming[5],incoming[6],incoming[7],incoming[8],incoming[9],incoming[10],incoming[11]};
       targetTime = atol(targetTimeInput); 
       currentShield = incoming[1] - '0' - 1;
       currentMotor  = incoming[2] - '0' - 1;
       motorStatus[currentShield][currentMotor][1] = incoming[3] - '0';
       EEPROM.put(eeAddress, motorStatus);
       
       motor[currentShield][currentMotor]->setRunTime(targetTime);

       //Always start perestaltic pump!
       //motor[0][1]->setSpeed(128); 
       //motor[0][1]->setRunTime(targetTime);
       //motor[0][1]->run(FORWARD);
       
       if (motorStatus[currentShield][currentMotor][1] == 1) {
         motor[currentShield][currentMotor]->run(FORWARD);
         startTime = millis();
         elapsedTime = 0;
         timeCounter = true;
         Serial.print("Started P: ");
         Serial.print(incoming[1]);
         Serial.print(",");
         Serial.print(incoming[2]);
         Serial.print(" in the fw dir. ");
         Serial.print("NSteps to go: ");
         Serial.print(motor[currentShield][currentMotor]->getTimeToGo());
         Serial.print(", time(ms):");
         Serial.print(targetTime);
         /*Serial.print(", speed:");
         Serial.print(motor[currentShield][currentMotor]->getSpeed());*/
         Serial.println();
       }
       else if (motorStatus[currentShield][currentMotor][1] == 2) {
         startTime = millis();
         elapsedTime = 0;
         timeCounter = true;
         motor[currentShield][currentMotor]->run(BACKWARD);
         Serial.print("Started P: ");
         Serial.print(incoming[1]);
         Serial.print(",");
         Serial.print(incoming[2]);
         Serial.print(" in the bw dir.");
         Serial.print("NSteps to go: ");
         Serial.print(motor[currentShield][currentMotor]->getTimeToGo());
         Serial.print(", time (ms):");
         Serial.print(targetTime);
         /*Serial.print(", speed:");
         Serial.print(motor[currentShield][currentMotor]->getSpeed());*/
         Serial.println();
       }else
       {
        Serial.println("invalid direction");
       }
     }     
     else {
        Serial.println("invalid command");
     }
  }  // end of process_data



  void processIncomingByte(const byte inByte)
  {
  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;

  switch (inByte)
    {
    case endMessage: //end of message marker
    case '\n':   // end of text
      input_line [input_pos] = 0;  // terminating null byte
      
      // terminator reached! process input_line here ...
      processSerialCmd(input_line, input_pos);
      
      // reset buffer for next time
      input_pos = 0;  
      break;
    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1))
        input_line [input_pos++] = inByte;
      break;

    }  // end of switch
   
  } // end of processIncomingByte  
