#include "SimpleMotor.h"

//Generic Motor Base Class***************************************************

SimpleMotor::SimpleMotor() {
  motorType=0;
  speedConversion=1;
}

uint8_t SimpleMotor::getType(void)
{
   return motorType;
}

void SimpleMotor::setSpeedConversion(float speedConversionIn)
{
   speedConversion=speedConversionIn;
}


//STEPPER Class***************************************************

SimpleStepper::SimpleStepper(Adafruit_MotorShield *MCShield, uint16_t steps, uint8_t n,float newSpeedConversion, uint8_t newAFStepStyle):AccelStepper{dummyFun, dummyFun} {
  AFStepper = MCShield->getStepper(steps, n);
  AFStepStyle=newAFStepStyle;
    
  motorType=SIMSTEPPER;
  speedConversion=newSpeedConversion;
  setMaxSpeed(MAX_STEP_SPEED);
  //isMaxSpeed=false;
}

void SimpleStepper::step(long step) //Overriding AccellStepper step function
{
    (void)(step); // Unused
    if (speed() > 0)
	    AFStepper->onestep(FORWARD, AFStepStyle); //MICROSTEP,SINGLE,INTERLEAVE ...
    else
	    AFStepper->onestep(BACKWARD, AFStepStyle);
}

void SimpleStepper::setSpeed(float newSpeed)
{
   stepperSpeed= newSpeed*speedConversion;
   AccelStepper::setSpeed(stepperSpeed);
   targetSteps=double(stepperSpeed)*targetRunTime/1000.;
   //isMaxSpeed = (newSpeed==255);
}

void SimpleStepper::setRunTime(long newRunTime)
{
   targetRunTime=newRunTime;
   targetSteps=double(stepperSpeed)*targetRunTime/1000.; 
}

void SimpleStepper::run(uint8_t cmd)
{
  //setCurrentPosition(0);
  float SpeedToSet = 0.;
  //if (isMaxSpeed)
  //    SpeedToSet=MAX_STEP_SPEED;
  //else
  SpeedToSet=stepperSpeed;
  
  move(targetSteps);
  switch (cmd) {
	case FORWARD:
    AccelStepper::setSpeed(SpeedToSet);
	StepperStatus=true;
    //Serial.println(targetSteps);
    break;
  case BACKWARD:
    AccelStepper::setSpeed(0-SpeedToSet);
	  StepperStatus=true;
    break;
  case RELEASE:
    AFStepper->release();
	  StepperStatus=false;
	  targetSteps=0;
  }
  //Serial.print("Run steps: ");
  // Serial.println(targetSteps);
}

bool SimpleStepper::doRunning()
{
   runSpeedToPosition();
   return(distanceToGo()==0);
}

float SimpleStepper::getTimeToGo(){
	
	return targetSteps;//distanceToGo();///stepperSpeed
}

float SimpleStepper::getSpeed(){
	
	return speed();
}


//DC-MOTOR Class***************************************************

SimpleDCMotor::SimpleDCMotor(Adafruit_MotorShield *MCShield, uint8_t num,float newSpeedConversion) {
  AFDCMotor = MCShield->getMotor(num);
  motorType=SIMDC;
  speedConversion=newSpeedConversion;
}

void SimpleDCMotor::setSpeed(float newSpeed)
{
   DCSpeed= uint8_t(newSpeed*speedConversion);
   AFDCMotor->setSpeed(DCSpeed);
}

void SimpleDCMotor::setRunTime(long newRunTime)
{
   targetRunTime=newRunTime;
}

void SimpleDCMotor::run(uint8_t cmd)
{
  AFDCMotor->setSpeed(DCSpeed);	
  startTime=millis();
  AFDCMotor->run(cmd);
  if (cmd==RELEASE){
      DCMotorStatus=false;	
    }
    else{
      DCMotorStatus=true;  
    }
}


bool SimpleDCMotor::doRunning()
{
   
   if (DCMotorStatus) {
	   elapsedTime = millis() - startTime;
		   
	   if (elapsedTime >=targetRunTime) {
		   AFDCMotor->run(RELEASE);
		   DCMotorStatus=false;
		   return true;
	   }
	   else {
		   return false;
	   }
   }
   else {
		return true;
   }
}

float SimpleDCMotor::getTimeToGo(){
	return float(targetRunTime-(millis() - startTime))/1000.;
}
float SimpleDCMotor::getSpeed(){
	return DCSpeed;
}
